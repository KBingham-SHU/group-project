﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Synthesis;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace group_poject_v2
{
    public partial class Form1 : Form
    {
        //Stores selected symbols
        IList<BlissSymbols> symbolsToOutput = new List<BlissSymbols>(); 
              
        //Stores all the symbols
        IList<BlissSymbols> allSymbolsObject = new List<BlissSymbols>();

        //Stores the current symbols on screen
        IList<BlissSymbols> currentSymbolsOnScreen = new List<BlissSymbols>();
        
        //Initialises the speech synthesizer       
        SpeechSynthesizer reader;

        //State of privacey mode false equals off
        bool privacyMute = false;

        //Number of icons displayed on sentence bar
        int displyCount = 0;

        //Used to access computer volume controls
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessageW(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);
        private const int APPCOMMAND_VOLUME_MUTE = 0x80000;
        private const int APPCOMMAND_VOLUME_UP = 0xA0000;
        private const int APPCOMMAND_VOLUME_DOWN = 0x90000;
        private const int WM_APPCOMMAND = 0x319;

        public Form1()
        {
            string allSymbolsJson;
            // Read JSON from file - BlissSymbols.json
            using (StreamReader r = new StreamReader("blissSymbols.json"))
            {
                allSymbolsJson = r.ReadToEnd();
            }

            // Parsing the JSON from string loaded from file
            JObject jsonSymbols = JObject.Parse(allSymbolsJson);

            // Spliting the each symbol and adding them into a list
            IList<JToken> symbols = jsonSymbols["symbols"].Children().ToList();
            // Loop through each JSON symbol in list
            foreach(JToken symbol in symbols)
            {
                // Convert the JSON into an object to be used by the code
                BlissSymbols tempSymbol = symbol.ToObject<BlissSymbols>();

                IList<JToken> jsonAssociation = symbol["Associated"].Children().ToList();

                IList<Associated> tempAssociated = new List<Associated>();
                foreach (JToken association in jsonAssociation)
                {
                    Associated tempAssociation = association.ToObject<Associated>();
                    tempAssociated.Add(tempAssociation);
                }
                // Storing the object of the symbol into a list
                tempSymbol.Associations = tempAssociated;
                allSymbolsObject.Add(tempSymbol);
            }

            InitializeComponent();

            //When form is loaded it loads group 0 symbols onto the form
            this.Shown += new System.EventHandler(this.Form1_Shown);
        }

        private void Form1_Shown(Object sender, EventArgs e)
        {
            //calls update symbols on screen function
            updateSymbols("0");
        }

        

        private void btnRead_Click(object sender, EventArgs e)
        {
            //Initialises an empty string
            string sentence = "";

            //For each symbol in sentence bar
            foreach(BlissSymbols symbol in symbolsToOutput)
            {
                //concatinate the english of each symbol
                sentence = sentence + " " + symbol.GetWord();
            }

            //sends the sentence to be ouputed by the synthisizer or message box
            outputSentence(sentence);
        }

        private void blissSymbolClick(object sender, EventArgs e)
        {
            string symbolClicked = "";
            string symbolID = "";
            string symbolGroup;

            if (sender is PictureBox)
            {
                symbolClicked = ((PictureBox)sender).Name;
            } else
            {
                var panels = (Panel)sender;
                symbolClicked = panels.Name;
            }
            
            symbolClicked = new String(symbolClicked.Where(Char.IsDigit).ToArray());
            int symbolNumber = int.Parse(symbolClicked);

            if (symbolNumber - 1 < currentSymbolsOnScreen.Count)
            {

                BlissSymbols clickedSymbol = currentSymbolsOnScreen[symbolNumber - 1];

                addSymbolToSpeechBar(clickedSymbol);
                symbolsToOutput.Add(clickedSymbol);

                symbolID = clickedSymbol.ID;
                symbolGroup = clickedSymbol.Group;

                updateSymbols(symbolGroup + 1, symbolID);
            }
        }

        private void volumeDecrease_Click(object sender, MouseEventArgs e)
        {
            SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle, (IntPtr)APPCOMMAND_VOLUME_DOWN);
        }

        private void volumeIncrease_Click(object sender, MouseEventArgs e)
        {
            SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle,(IntPtr)APPCOMMAND_VOLUME_UP);
        }

        private void mute_Click(object sender, EventArgs e)
        {
            if(privacyMute == false)
            {
                outputSentence("Please read the screen from this point onward, I want to tell you something in private.");
                privacyMute = true;
            }
            else
            {
                privacyMute = false;
                outputSentence("I have finished talking to you in private.");
            }
        }

        private void settings_Click(object sender, EventArgs e)
        {

        }

        private void assistance_Click(object sender, EventArgs e)
        {

        }

        private void privacey_Click(object sender, EventArgs e)
        {

        }

        private void updateSymbols(string group, string ID = "")
        {
            PictureBox[] boxes = {
                blissSymbol1,
                blissSymbol2,
                blissSymbol3,
                blissSymbol4,
                blissSymbol5,
                blissSymbol6,
                blissSymbol7,
                blissSymbol8,
                blissSymbol9,
                blissSymbol10,
                blissSymbol11,
                blissSymbol12,
                blissSymbol13,
                blissSymbol14,
                blissSymbol15,
                blissSymbol16,
                blissSymbol17,
                blissSymbol18,
                blissSymbol19,
                blissSymbol20,
                blissSymbol21,
                blissSymbol22,
                blissSymbol23,
                blissSymbol24
            };

            int count = 0;
            currentSymbolsOnScreen.Clear();
            clearSymbols(boxes);

            
            if (ID != "")
            {
                foreach (BlissSymbols symbol in allSymbolsObject)
                {
                    foreach (Associated associated in symbol.Associations)
                    {
                        if (associated.ID == ID)
                        {
                            boxes[count].ImageLocation = "./symbols/" + symbol.English + ".png";
                            boxes[count].SizeMode = PictureBoxSizeMode.AutoSize;
                            boxes[count].Anchor = AnchorStyles.None;
                            boxes[count].Width = 100;
                            boxes[count].Load();
                            count++;
                            currentSymbolsOnScreen.Add(symbol);

                            symbol.SelectedPosition = int.Parse(associated.Position);
                        }
                    }
                }  
            } else
            {
                foreach (BlissSymbols symbol in allSymbolsObject)
                {
                    if (symbol.Group == group)
                    {
                        boxes[count].ImageLocation = "./symbols/" + symbol.English + ".png";
                        boxes[count].SizeMode = PictureBoxSizeMode.AutoSize;
                        boxes[count].Anchor = AnchorStyles.None;
                        boxes[count].Width = 100;
                        boxes[count].Load();
                        count++;
                        currentSymbolsOnScreen.Add(symbol);
                    }
                }
            }
        }

        private void clearSymbols(PictureBox[] boxes)
        {
            foreach (PictureBox box in boxes)
            {
                box.ImageLocation = "./images/blank.png";
                box.SizeMode = PictureBoxSizeMode.AutoSize;
                box.Anchor = AnchorStyles.None;
                box.Load();
            }
        }

        private void outputSentence(string sentence)
        {
            if(privacyMute == false)
            {
                reader = new SpeechSynthesizer();
                reader.SpeakAsync(sentence);
            } else
            {
                string caption = "Private message";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                
                result = MessageBox.Show(sentence, caption, buttons);
            }
            
        }

        private void addSymbolToSpeechBar(BlissSymbols symbol)
        {
            PictureBox[] boxes = {
                displayBliss1,
                displayBliss2,
                displayBliss3,
                displayBliss4,
                displayBliss5,
                displayBliss6,
                displayBliss7
            };
            if (displyCount < 7)
            {
                boxes[displyCount].ImageLocation = "./symbolsSmall/" + symbol.English + ".png";
                boxes[displyCount].SizeMode = PictureBoxSizeMode.AutoSize;
                boxes[displyCount].Anchor = AnchorStyles.None;
                boxes[displyCount].Load();
                displyCount++;
            } else
            {
                string message = "Too many symbols on display bar";
                string caption = "To Many symbols";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                result = MessageBox.Show(message, caption, buttons);
            }
            
        }

        private void clearMessage_Click(object sender, EventArgs e)
        {
            PictureBox[] boxes = {
                displayBliss1,
                displayBliss2,
                displayBliss3,
                displayBliss4,
                displayBliss5,
                displayBliss6,
                displayBliss7
            };

            for(int i = 0; i < 7; i++)
            {
                boxes[i].ImageLocation = "./images/blank.png";
                boxes[i].SizeMode = PictureBoxSizeMode.AutoSize;
                boxes[i].Anchor = AnchorStyles.None;
                boxes[i].Load();
            }
            displyCount = 0;
            symbolsToOutput.Clear();
            updateSymbols("0");

        }

        private void blissSymbolHover(object sender, EventArgs e)
        {

        }
    }
}
