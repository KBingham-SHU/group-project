﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace group_poject_v2
{
    class Bitwise
    {
        public int ID { get; set; }
        public string English { get; set; }
        public string POS { get; set; }
        public string Derivation { get; set; }

    }
}
