﻿namespace group_poject_v2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.blissSymbol1 = new System.Windows.Forms.PictureBox();
            this.blissSymbol2 = new System.Windows.Forms.PictureBox();
            this.blissSymbol3 = new System.Windows.Forms.PictureBox();
            this.blissSymbol4 = new System.Windows.Forms.PictureBox();
            this.blissSymbol5 = new System.Windows.Forms.PictureBox();
            this.blissSymbol6 = new System.Windows.Forms.PictureBox();
            this.blissSymbol7 = new System.Windows.Forms.PictureBox();
            this.blissSymbol8 = new System.Windows.Forms.PictureBox();
            this.blissSymbol9 = new System.Windows.Forms.PictureBox();
            this.blissSymbol11 = new System.Windows.Forms.PictureBox();
            this.blissSymbol12 = new System.Windows.Forms.PictureBox();
            this.blissSymbol13 = new System.Windows.Forms.PictureBox();
            this.blissSymbol14 = new System.Windows.Forms.PictureBox();
            this.blissSymbol15 = new System.Windows.Forms.PictureBox();
            this.blissSymbol16 = new System.Windows.Forms.PictureBox();
            this.blissSymbol17 = new System.Windows.Forms.PictureBox();
            this.blissSymbol18 = new System.Windows.Forms.PictureBox();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.blissSymbol19 = new System.Windows.Forms.PictureBox();
            this.blissSymbol21 = new System.Windows.Forms.PictureBox();
            this.blissSymbol22 = new System.Windows.Forms.PictureBox();
            this.blissSymbol23 = new System.Windows.Forms.PictureBox();
            this.blissSymbol24 = new System.Windows.Forms.PictureBox();
            this.playSymbol = new System.Windows.Forms.PictureBox();
            this.volumeDecrease = new System.Windows.Forms.PictureBox();
            this.volumeIncrease = new System.Windows.Forms.PictureBox();
            this.mute = new System.Windows.Forms.PictureBox();
            this.settings = new System.Windows.Forms.PictureBox();
            this.assistance = new System.Windows.Forms.PictureBox();
            this.privacey = new System.Windows.Forms.PictureBox();
            this.clearMessage = new System.Windows.Forms.PictureBox();
            this.displayBliss1 = new System.Windows.Forms.PictureBox();
            this.displayBliss2 = new System.Windows.Forms.PictureBox();
            this.displayBliss3 = new System.Windows.Forms.PictureBox();
            this.displayBliss4 = new System.Windows.Forms.PictureBox();
            this.displayBliss5 = new System.Windows.Forms.PictureBox();
            this.displayBliss6 = new System.Windows.Forms.PictureBox();
            this.displayBliss7 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.symbol1 = new System.Windows.Forms.Panel();
            this.symbol2 = new System.Windows.Forms.Panel();
            this.symbol3 = new System.Windows.Forms.Panel();
            this.symbol4 = new System.Windows.Forms.Panel();
            this.symbol5 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.symbol8 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.symbol11 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.symbol15 = new System.Windows.Forms.Panel();
            this.symbol16 = new System.Windows.Forms.Panel();
            this.symbol17 = new System.Windows.Forms.Panel();
            this.symbol19 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.symbol6 = new System.Windows.Forms.Panel();
            this.symbol7 = new System.Windows.Forms.Panel();
            this.symbol9 = new System.Windows.Forms.Panel();
            this.symbol10 = new System.Windows.Forms.Panel();
            this.blissSymbol10 = new System.Windows.Forms.PictureBox();
            this.symbol12 = new System.Windows.Forms.Panel();
            this.symbol13 = new System.Windows.Forms.Panel();
            this.symbol14 = new System.Windows.Forms.Panel();
            this.blissSymbol20 = new System.Windows.Forms.PictureBox();
            this.symbol20 = new System.Windows.Forms.PictureBox();
            this.symbol18 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playSymbol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeDecrease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeIncrease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.assistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.privacey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss7)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.symbol3.SuspendLayout();
            this.symbol4.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel17.SuspendLayout();
            this.symbol17.SuspendLayout();
            this.symbol19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.symbol7.SuspendLayout();
            this.symbol9.SuspendLayout();
            this.symbol10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol10)).BeginInit();
            this.symbol12.SuspendLayout();
            this.symbol13.SuspendLayout();
            this.symbol14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.symbol20)).BeginInit();
            this.symbol18.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // blissSymbol1
            // 
            this.blissSymbol1.BackColor = System.Drawing.Color.White;
            this.blissSymbol1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.blissSymbol1.InitialImage = null;
            this.blissSymbol1.Location = new System.Drawing.Point(12, 122);
            this.blissSymbol1.Name = "blissSymbol1";
            this.blissSymbol1.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol1.TabIndex = 2;
            this.blissSymbol1.TabStop = false;
            this.blissSymbol1.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol2
            // 
            this.blissSymbol2.BackColor = System.Drawing.Color.White;
            this.blissSymbol2.Location = new System.Drawing.Point(144, 122);
            this.blissSymbol2.Name = "blissSymbol2";
            this.blissSymbol2.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol2.TabIndex = 2;
            this.blissSymbol2.TabStop = false;
            this.blissSymbol2.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol3
            // 
            this.blissSymbol3.BackColor = System.Drawing.Color.White;
            this.blissSymbol3.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol3.Name = "blissSymbol3";
            this.blissSymbol3.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol3.TabIndex = 2;
            this.blissSymbol3.TabStop = false;
            this.blissSymbol3.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol4
            // 
            this.blissSymbol4.BackColor = System.Drawing.Color.White;
            this.blissSymbol4.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol4.Name = "blissSymbol4";
            this.blissSymbol4.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol4.TabIndex = 2;
            this.blissSymbol4.TabStop = false;
            this.blissSymbol4.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol5
            // 
            this.blissSymbol5.BackColor = System.Drawing.Color.White;
            this.blissSymbol5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.blissSymbol5.Location = new System.Drawing.Point(540, 122);
            this.blissSymbol5.Name = "blissSymbol5";
            this.blissSymbol5.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol5.TabIndex = 2;
            this.blissSymbol5.TabStop = false;
            this.blissSymbol5.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol6
            // 
            this.blissSymbol6.BackColor = System.Drawing.Color.White;
            this.blissSymbol6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.blissSymbol6.Location = new System.Drawing.Point(672, 122);
            this.blissSymbol6.Name = "blissSymbol6";
            this.blissSymbol6.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol6.TabIndex = 2;
            this.blissSymbol6.TabStop = false;
            this.blissSymbol6.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol7
            // 
            this.blissSymbol7.BackColor = System.Drawing.Color.White;
            this.blissSymbol7.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol7.Name = "blissSymbol7";
            this.blissSymbol7.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol7.TabIndex = 2;
            this.blissSymbol7.TabStop = false;
            this.blissSymbol7.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol8
            // 
            this.blissSymbol8.BackColor = System.Drawing.Color.White;
            this.blissSymbol8.Location = new System.Drawing.Point(144, 251);
            this.blissSymbol8.Name = "blissSymbol8";
            this.blissSymbol8.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol8.TabIndex = 2;
            this.blissSymbol8.TabStop = false;
            this.blissSymbol8.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol9
            // 
            this.blissSymbol9.BackColor = System.Drawing.Color.White;
            this.blissSymbol9.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol9.Name = "blissSymbol9";
            this.blissSymbol9.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol9.TabIndex = 2;
            this.blissSymbol9.TabStop = false;
            this.blissSymbol9.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol11
            // 
            this.blissSymbol11.BackColor = System.Drawing.Color.White;
            this.blissSymbol11.Location = new System.Drawing.Point(540, 251);
            this.blissSymbol11.Name = "blissSymbol11";
            this.blissSymbol11.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol11.TabIndex = 2;
            this.blissSymbol11.TabStop = false;
            this.blissSymbol11.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol12
            // 
            this.blissSymbol12.BackColor = System.Drawing.Color.White;
            this.blissSymbol12.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol12.Name = "blissSymbol12";
            this.blissSymbol12.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol12.TabIndex = 2;
            this.blissSymbol12.TabStop = false;
            this.blissSymbol12.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol13
            // 
            this.blissSymbol13.BackColor = System.Drawing.Color.White;
            this.blissSymbol13.Location = new System.Drawing.Point(0, -1);
            this.blissSymbol13.Name = "blissSymbol13";
            this.blissSymbol13.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol13.TabIndex = 2;
            this.blissSymbol13.TabStop = false;
            this.blissSymbol13.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol14
            // 
            this.blissSymbol14.BackColor = System.Drawing.Color.White;
            this.blissSymbol14.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol14.Name = "blissSymbol14";
            this.blissSymbol14.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol14.TabIndex = 2;
            this.blissSymbol14.TabStop = false;
            this.blissSymbol14.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol15
            // 
            this.blissSymbol15.BackColor = System.Drawing.Color.White;
            this.blissSymbol15.Location = new System.Drawing.Point(276, 380);
            this.blissSymbol15.Name = "blissSymbol15";
            this.blissSymbol15.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol15.TabIndex = 2;
            this.blissSymbol15.TabStop = false;
            this.blissSymbol15.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol16
            // 
            this.blissSymbol16.BackColor = System.Drawing.Color.White;
            this.blissSymbol16.Location = new System.Drawing.Point(408, 380);
            this.blissSymbol16.Name = "blissSymbol16";
            this.blissSymbol16.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol16.TabIndex = 2;
            this.blissSymbol16.TabStop = false;
            this.blissSymbol16.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol17
            // 
            this.blissSymbol17.BackColor = System.Drawing.Color.White;
            this.blissSymbol17.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol17.Name = "blissSymbol17";
            this.blissSymbol17.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol17.TabIndex = 2;
            this.blissSymbol17.TabStop = false;
            this.blissSymbol17.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol18
            // 
            this.blissSymbol18.BackColor = System.Drawing.Color.White;
            this.blissSymbol18.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol18.Name = "blissSymbol18";
            this.blissSymbol18.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol18.TabIndex = 2;
            this.blissSymbol18.TabStop = false;
            this.blissSymbol18.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol19
            // 
            this.blissSymbol19.BackColor = System.Drawing.Color.White;
            this.blissSymbol19.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol19.Name = "blissSymbol19";
            this.blissSymbol19.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol19.TabIndex = 2;
            this.blissSymbol19.TabStop = false;
            this.blissSymbol19.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol21
            // 
            this.blissSymbol21.BackColor = System.Drawing.Color.White;
            this.blissSymbol21.Location = new System.Drawing.Point(276, 509);
            this.blissSymbol21.Name = "blissSymbol21";
            this.blissSymbol21.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol21.TabIndex = 2;
            this.blissSymbol21.TabStop = false;
            this.blissSymbol21.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol22
            // 
            this.blissSymbol22.BackColor = System.Drawing.Color.White;
            this.blissSymbol22.Location = new System.Drawing.Point(408, 509);
            this.blissSymbol22.Name = "blissSymbol22";
            this.blissSymbol22.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol22.TabIndex = 2;
            this.blissSymbol22.TabStop = false;
            this.blissSymbol22.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol23
            // 
            this.blissSymbol23.BackColor = System.Drawing.Color.White;
            this.blissSymbol23.Location = new System.Drawing.Point(540, 509);
            this.blissSymbol23.Name = "blissSymbol23";
            this.blissSymbol23.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol23.TabIndex = 2;
            this.blissSymbol23.TabStop = false;
            this.blissSymbol23.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // blissSymbol24
            // 
            this.blissSymbol24.BackColor = System.Drawing.Color.White;
            this.blissSymbol24.Location = new System.Drawing.Point(672, 509);
            this.blissSymbol24.Name = "blissSymbol24";
            this.blissSymbol24.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol24.TabIndex = 2;
            this.blissSymbol24.TabStop = false;
            this.blissSymbol24.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // playSymbol
            // 
            this.playSymbol.BackColor = System.Drawing.Color.White;
            this.playSymbol.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.playSymbol.Image = ((System.Drawing.Image)(resources.GetObject("playSymbol.Image")));
            this.playSymbol.Location = new System.Drawing.Point(869, 12);
            this.playSymbol.Name = "playSymbol";
            this.playSymbol.Size = new System.Drawing.Size(101, 104);
            this.playSymbol.TabIndex = 2;
            this.playSymbol.TabStop = false;
            this.playSymbol.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // volumeDecrease
            // 
            this.volumeDecrease.BackColor = System.Drawing.Color.White;
            this.volumeDecrease.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.volumeDecrease.Image = ((System.Drawing.Image)(resources.GetObject("volumeDecrease.Image")));
            this.volumeDecrease.Location = new System.Drawing.Point(3, 7);
            this.volumeDecrease.Name = "volumeDecrease";
            this.volumeDecrease.Size = new System.Drawing.Size(80, 80);
            this.volumeDecrease.TabIndex = 2;
            this.volumeDecrease.TabStop = false;
            this.volumeDecrease.MouseDown += new System.Windows.Forms.MouseEventHandler(this.volumeDecrease_Click);
            // 
            // volumeIncrease
            // 
            this.volumeIncrease.BackColor = System.Drawing.Color.White;
            this.volumeIncrease.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.volumeIncrease.Image = ((System.Drawing.Image)(resources.GetObject("volumeIncrease.Image")));
            this.volumeIncrease.Location = new System.Drawing.Point(890, 122);
            this.volumeIncrease.Name = "volumeIncrease";
            this.volumeIncrease.Size = new System.Drawing.Size(80, 80);
            this.volumeIncrease.TabIndex = 2;
            this.volumeIncrease.TabStop = false;
            this.volumeIncrease.MouseDown += new System.Windows.Forms.MouseEventHandler(this.volumeIncrease_Click);
            // 
            // mute
            // 
            this.mute.BackColor = System.Drawing.Color.White;
            this.mute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.mute.Image = ((System.Drawing.Image)(resources.GetObject("mute.Image")));
            this.mute.Location = new System.Drawing.Point(3, 93);
            this.mute.Name = "mute";
            this.mute.Size = new System.Drawing.Size(80, 80);
            this.mute.TabIndex = 2;
            this.mute.TabStop = false;
            this.mute.Click += new System.EventHandler(this.mute_Click);
            // 
            // settings
            // 
            this.settings.BackColor = System.Drawing.Color.White;
            this.settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.settings.Image = ((System.Drawing.Image)(resources.GetObject("settings.Image")));
            this.settings.Location = new System.Drawing.Point(890, 208);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(80, 80);
            this.settings.TabIndex = 2;
            this.settings.TabStop = false;
            this.settings.Click += new System.EventHandler(this.settings_Click);
            // 
            // assistance
            // 
            this.assistance.BackColor = System.Drawing.Color.White;
            this.assistance.Image = ((System.Drawing.Image)(resources.GetObject("assistance.Image")));
            this.assistance.Location = new System.Drawing.Point(804, 294);
            this.assistance.Name = "assistance";
            this.assistance.Size = new System.Drawing.Size(166, 145);
            this.assistance.TabIndex = 2;
            this.assistance.TabStop = false;
            this.assistance.Click += new System.EventHandler(this.assistance_Click);
            // 
            // privacey
            // 
            this.privacey.BackColor = System.Drawing.Color.White;
            this.privacey.Image = ((System.Drawing.Image)(resources.GetObject("privacey.Image")));
            this.privacey.Location = new System.Drawing.Point(804, 445);
            this.privacey.Name = "privacey";
            this.privacey.Size = new System.Drawing.Size(166, 166);
            this.privacey.TabIndex = 2;
            this.privacey.TabStop = false;
            this.privacey.Click += new System.EventHandler(this.privacey_Click);
            // 
            // clearMessage
            // 
            this.clearMessage.BackColor = System.Drawing.Color.White;
            this.clearMessage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.clearMessage.Image = ((System.Drawing.Image)(resources.GetObject("clearMessage.Image")));
            this.clearMessage.Location = new System.Drawing.Point(762, 12);
            this.clearMessage.Name = "clearMessage";
            this.clearMessage.Size = new System.Drawing.Size(101, 104);
            this.clearMessage.TabIndex = 2;
            this.clearMessage.TabStop = false;
            this.clearMessage.Click += new System.EventHandler(this.clearMessage_Click);
            // 
            // displayBliss1
            // 
            this.displayBliss1.Location = new System.Drawing.Point(7, 13);
            this.displayBliss1.Name = "displayBliss1";
            this.displayBliss1.Size = new System.Drawing.Size(100, 78);
            this.displayBliss1.TabIndex = 3;
            this.displayBliss1.TabStop = false;
            // 
            // displayBliss2
            // 
            this.displayBliss2.Location = new System.Drawing.Point(113, 13);
            this.displayBliss2.Name = "displayBliss2";
            this.displayBliss2.Size = new System.Drawing.Size(100, 78);
            this.displayBliss2.TabIndex = 3;
            this.displayBliss2.TabStop = false;
            // 
            // displayBliss3
            // 
            this.displayBliss3.Location = new System.Drawing.Point(219, 13);
            this.displayBliss3.Name = "displayBliss3";
            this.displayBliss3.Size = new System.Drawing.Size(100, 78);
            this.displayBliss3.TabIndex = 3;
            this.displayBliss3.TabStop = false;
            // 
            // displayBliss4
            // 
            this.displayBliss4.Location = new System.Drawing.Point(325, 13);
            this.displayBliss4.Name = "displayBliss4";
            this.displayBliss4.Size = new System.Drawing.Size(100, 78);
            this.displayBliss4.TabIndex = 3;
            this.displayBliss4.TabStop = false;
            // 
            // displayBliss5
            // 
            this.displayBliss5.Location = new System.Drawing.Point(431, 13);
            this.displayBliss5.Name = "displayBliss5";
            this.displayBliss5.Size = new System.Drawing.Size(100, 78);
            this.displayBliss5.TabIndex = 3;
            this.displayBliss5.TabStop = false;
            // 
            // displayBliss6
            // 
            this.displayBliss6.Location = new System.Drawing.Point(537, 13);
            this.displayBliss6.Name = "displayBliss6";
            this.displayBliss6.Size = new System.Drawing.Size(100, 78);
            this.displayBliss6.TabIndex = 3;
            this.displayBliss6.TabStop = false;
            // 
            // displayBliss7
            // 
            this.displayBliss7.Location = new System.Drawing.Point(643, 13);
            this.displayBliss7.Name = "displayBliss7";
            this.displayBliss7.Size = new System.Drawing.Size(100, 78);
            this.displayBliss7.TabIndex = 3;
            this.displayBliss7.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.displayBliss7);
            this.panel1.Controls.Add(this.displayBliss1);
            this.panel1.Controls.Add(this.displayBliss6);
            this.panel1.Controls.Add(this.displayBliss2);
            this.panel1.Controls.Add(this.displayBliss5);
            this.panel1.Controls.Add(this.displayBliss3);
            this.panel1.Controls.Add(this.displayBliss4);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(958, 104);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.mute);
            this.panel2.Controls.Add(this.volumeDecrease);
            this.panel2.Location = new System.Drawing.Point(804, 115);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(166, 516);
            this.panel2.TabIndex = 5;
            // 
            // symbol1
            // 
            this.symbol1.BackColor = System.Drawing.Color.White;
            this.symbol1.Location = new System.Drawing.Point(12, 122);
            this.symbol1.Name = "symbol1";
            this.symbol1.Size = new System.Drawing.Size(126, 123);
            this.symbol1.TabIndex = 6;
            this.symbol1.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol1.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol2
            // 
            this.symbol2.BackColor = System.Drawing.Color.White;
            this.symbol2.Location = new System.Drawing.Point(144, 122);
            this.symbol2.Name = "symbol2";
            this.symbol2.Size = new System.Drawing.Size(126, 123);
            this.symbol2.TabIndex = 6;
            this.symbol2.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol2.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol3
            // 
            this.symbol3.BackColor = System.Drawing.Color.White;
            this.symbol3.Controls.Add(this.blissSymbol3);
            this.symbol3.Location = new System.Drawing.Point(276, 122);
            this.symbol3.Name = "symbol3";
            this.symbol3.Size = new System.Drawing.Size(126, 123);
            this.symbol3.TabIndex = 6;
            this.symbol3.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol3.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol4
            // 
            this.symbol4.BackColor = System.Drawing.Color.White;
            this.symbol4.Controls.Add(this.blissSymbol4);
            this.symbol4.Location = new System.Drawing.Point(408, 122);
            this.symbol4.Name = "symbol4";
            this.symbol4.Size = new System.Drawing.Size(126, 123);
            this.symbol4.TabIndex = 6;
            this.symbol4.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol4.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol5
            // 
            this.symbol5.BackColor = System.Drawing.Color.White;
            this.symbol5.Location = new System.Drawing.Point(540, 122);
            this.symbol5.Name = "symbol5";
            this.symbol5.Size = new System.Drawing.Size(126, 123);
            this.symbol5.TabIndex = 6;
            this.symbol5.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol5.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.Controls.Add(this.symbol8);
            this.panel10.Location = new System.Drawing.Point(144, 251);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(126, 123);
            this.panel10.TabIndex = 6;
            // 
            // symbol8
            // 
            this.symbol8.BackColor = System.Drawing.Color.White;
            this.symbol8.Location = new System.Drawing.Point(0, 0);
            this.symbol8.Name = "symbol8";
            this.symbol8.Size = new System.Drawing.Size(126, 123);
            this.symbol8.TabIndex = 6;
            this.symbol8.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol8.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.White;
            this.panel13.Controls.Add(this.symbol11);
            this.panel13.Location = new System.Drawing.Point(540, 251);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(126, 123);
            this.panel13.TabIndex = 6;
            // 
            // symbol11
            // 
            this.symbol11.BackColor = System.Drawing.Color.White;
            this.symbol11.Location = new System.Drawing.Point(0, 0);
            this.symbol11.Name = "symbol11";
            this.symbol11.Size = new System.Drawing.Size(126, 123);
            this.symbol11.TabIndex = 6;
            this.symbol11.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol11.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.White;
            this.panel17.Controls.Add(this.symbol15);
            this.panel17.Location = new System.Drawing.Point(276, 380);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(126, 123);
            this.panel17.TabIndex = 6;
            // 
            // symbol15
            // 
            this.symbol15.BackColor = System.Drawing.Color.White;
            this.symbol15.Location = new System.Drawing.Point(0, 0);
            this.symbol15.Name = "symbol15";
            this.symbol15.Size = new System.Drawing.Size(126, 123);
            this.symbol15.TabIndex = 6;
            this.symbol15.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol15.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol16
            // 
            this.symbol16.BackColor = System.Drawing.Color.White;
            this.symbol16.Location = new System.Drawing.Point(408, 380);
            this.symbol16.Name = "symbol16";
            this.symbol16.Size = new System.Drawing.Size(126, 123);
            this.symbol16.TabIndex = 6;
            this.symbol16.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol16.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol17
            // 
            this.symbol17.BackColor = System.Drawing.Color.White;
            this.symbol17.Controls.Add(this.blissSymbol17);
            this.symbol17.Location = new System.Drawing.Point(540, 380);
            this.symbol17.Name = "symbol17";
            this.symbol17.Size = new System.Drawing.Size(126, 123);
            this.symbol17.TabIndex = 6;
            this.symbol17.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol17.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol19
            // 
            this.symbol19.BackColor = System.Drawing.Color.White;
            this.symbol19.Controls.Add(this.blissSymbol19);
            this.symbol19.Location = new System.Drawing.Point(12, 508);
            this.symbol19.Name = "symbol19";
            this.symbol19.Size = new System.Drawing.Size(126, 123);
            this.symbol19.TabIndex = 6;
            this.symbol19.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol19.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.White;
            this.panel23.Location = new System.Drawing.Point(276, 509);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(126, 123);
            this.panel23.TabIndex = 6;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Location = new System.Drawing.Point(276, 509);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(126, 123);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.White;
            this.panel24.Location = new System.Drawing.Point(408, 509);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(126, 123);
            this.panel24.TabIndex = 6;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.White;
            this.pictureBox4.Location = new System.Drawing.Point(408, 509);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(126, 123);
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.White;
            this.panel25.Location = new System.Drawing.Point(540, 509);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(126, 123);
            this.panel25.TabIndex = 6;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.White;
            this.pictureBox5.Location = new System.Drawing.Point(540, 509);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(126, 123);
            this.pictureBox5.TabIndex = 2;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.White;
            this.panel26.Location = new System.Drawing.Point(672, 509);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(126, 123);
            this.panel26.TabIndex = 6;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.White;
            this.pictureBox6.Location = new System.Drawing.Point(672, 509);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(126, 123);
            this.pictureBox6.TabIndex = 2;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // symbol6
            // 
            this.symbol6.BackColor = System.Drawing.Color.White;
            this.symbol6.Location = new System.Drawing.Point(672, 122);
            this.symbol6.Name = "symbol6";
            this.symbol6.Size = new System.Drawing.Size(126, 123);
            this.symbol6.TabIndex = 6;
            this.symbol6.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol6.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol7
            // 
            this.symbol7.BackColor = System.Drawing.Color.White;
            this.symbol7.Controls.Add(this.blissSymbol7);
            this.symbol7.Location = new System.Drawing.Point(12, 251);
            this.symbol7.Name = "symbol7";
            this.symbol7.Size = new System.Drawing.Size(126, 123);
            this.symbol7.TabIndex = 6;
            this.symbol7.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol7.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol9
            // 
            this.symbol9.BackColor = System.Drawing.Color.White;
            this.symbol9.Controls.Add(this.blissSymbol9);
            this.symbol9.Location = new System.Drawing.Point(276, 251);
            this.symbol9.Name = "symbol9";
            this.symbol9.Size = new System.Drawing.Size(126, 123);
            this.symbol9.TabIndex = 6;
            this.symbol9.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol9.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol10
            // 
            this.symbol10.BackColor = System.Drawing.Color.White;
            this.symbol10.Controls.Add(this.blissSymbol10);
            this.symbol10.Location = new System.Drawing.Point(408, 251);
            this.symbol10.Name = "symbol10";
            this.symbol10.Size = new System.Drawing.Size(126, 123);
            this.symbol10.TabIndex = 6;
            this.symbol10.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol10.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // blissSymbol10
            // 
            this.blissSymbol10.BackColor = System.Drawing.Color.White;
            this.blissSymbol10.Location = new System.Drawing.Point(0, 0);
            this.blissSymbol10.Name = "blissSymbol10";
            this.blissSymbol10.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol10.TabIndex = 2;
            this.blissSymbol10.TabStop = false;
            this.blissSymbol10.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // symbol12
            // 
            this.symbol12.BackColor = System.Drawing.Color.White;
            this.symbol12.Controls.Add(this.blissSymbol12);
            this.symbol12.Location = new System.Drawing.Point(672, 251);
            this.symbol12.Name = "symbol12";
            this.symbol12.Size = new System.Drawing.Size(126, 123);
            this.symbol12.TabIndex = 6;
            this.symbol12.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol12.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol13
            // 
            this.symbol13.BackColor = System.Drawing.Color.White;
            this.symbol13.Controls.Add(this.blissSymbol13);
            this.symbol13.Location = new System.Drawing.Point(12, 380);
            this.symbol13.Name = "symbol13";
            this.symbol13.Size = new System.Drawing.Size(126, 123);
            this.symbol13.TabIndex = 6;
            this.symbol13.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol13.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol14
            // 
            this.symbol14.BackColor = System.Drawing.Color.White;
            this.symbol14.Controls.Add(this.blissSymbol14);
            this.symbol14.Location = new System.Drawing.Point(144, 380);
            this.symbol14.Name = "symbol14";
            this.symbol14.Size = new System.Drawing.Size(126, 123);
            this.symbol14.TabIndex = 6;
            this.symbol14.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol14.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // blissSymbol20
            // 
            this.blissSymbol20.BackColor = System.Drawing.Color.White;
            this.blissSymbol20.Location = new System.Drawing.Point(144, 508);
            this.blissSymbol20.Name = "blissSymbol20";
            this.blissSymbol20.Size = new System.Drawing.Size(126, 123);
            this.blissSymbol20.TabIndex = 2;
            this.blissSymbol20.TabStop = false;
            this.blissSymbol20.Click += new System.EventHandler(this.blissSymbolClick);
            // 
            // symbol20
            // 
            this.symbol20.BackColor = System.Drawing.Color.White;
            this.symbol20.Location = new System.Drawing.Point(144, 509);
            this.symbol20.Name = "symbol20";
            this.symbol20.Size = new System.Drawing.Size(126, 123);
            this.symbol20.TabIndex = 2;
            this.symbol20.TabStop = false;
            this.symbol20.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol20.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // symbol18
            // 
            this.symbol18.BackColor = System.Drawing.Color.White;
            this.symbol18.Controls.Add(this.blissSymbol18);
            this.symbol18.Location = new System.Drawing.Point(0, 0);
            this.symbol18.Name = "symbol18";
            this.symbol18.Size = new System.Drawing.Size(126, 123);
            this.symbol18.TabIndex = 6;
            this.symbol18.Click += new System.EventHandler(this.blissSymbolClick);
            this.symbol18.MouseHover += new System.EventHandler(this.blissSymbolHover);
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.White;
            this.panel20.Controls.Add(this.symbol18);
            this.panel20.Location = new System.Drawing.Point(672, 380);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(126, 123);
            this.panel20.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 653);
            this.Controls.Add(this.blissSymbol16);
            this.Controls.Add(this.blissSymbol24);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.privacey);
            this.Controls.Add(this.assistance);
            this.Controls.Add(this.blissSymbol23);
            this.Controls.Add(this.volumeIncrease);
            this.Controls.Add(this.settings);
            this.Controls.Add(this.clearMessage);
            this.Controls.Add(this.playSymbol);
            this.Controls.Add(this.blissSymbol11);
            this.Controls.Add(this.blissSymbol22);
            this.Controls.Add(this.blissSymbol21);
            this.Controls.Add(this.blissSymbol15);
            this.Controls.Add(this.blissSymbol20);
            this.Controls.Add(this.blissSymbol8);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.symbol19);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.symbol7);
            this.Controls.Add(this.symbol10);
            this.Controls.Add(this.symbol9);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.symbol20);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.blissSymbol2);
            this.Controls.Add(this.symbol2);
            this.Controls.Add(this.blissSymbol1);
            this.Controls.Add(this.symbol1);
            this.Controls.Add(this.symbol4);
            this.Controls.Add(this.blissSymbol5);
            this.Controls.Add(this.blissSymbol6);
            this.Controls.Add(this.symbol5);
            this.Controls.Add(this.symbol6);
            this.Controls.Add(this.symbol12);
            this.Controls.Add(this.symbol14);
            this.Controls.Add(this.symbol13);
            this.Controls.Add(this.symbol17);
            this.Controls.Add(this.symbol16);
            this.Controls.Add(this.symbol3);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playSymbol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeDecrease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeIncrease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.assistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.privacey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayBliss7)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.symbol3.ResumeLayout(false);
            this.symbol4.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.symbol17.ResumeLayout(false);
            this.symbol19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.symbol7.ResumeLayout(false);
            this.symbol9.ResumeLayout(false);
            this.symbol10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol10)).EndInit();
            this.symbol12.ResumeLayout(false);
            this.symbol13.ResumeLayout(false);
            this.symbol14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.blissSymbol20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.symbol20)).EndInit();
            this.symbol18.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox blissSymbol1;
        private System.Windows.Forms.PictureBox blissSymbol2;
        private System.Windows.Forms.PictureBox blissSymbol3;
        private System.Windows.Forms.PictureBox blissSymbol4;
        private System.Windows.Forms.PictureBox blissSymbol5;
        private System.Windows.Forms.PictureBox blissSymbol6;
        private System.Windows.Forms.PictureBox blissSymbol7;
        private System.Windows.Forms.PictureBox blissSymbol8;
        private System.Windows.Forms.PictureBox blissSymbol9;
        private System.Windows.Forms.PictureBox blissSymbol11;
        private System.Windows.Forms.PictureBox blissSymbol12;
        private System.Windows.Forms.PictureBox blissSymbol13;
        private System.Windows.Forms.PictureBox blissSymbol14;
        private System.Windows.Forms.PictureBox blissSymbol15;
        private System.Windows.Forms.PictureBox blissSymbol16;
        private System.Windows.Forms.PictureBox blissSymbol17;
        private System.Windows.Forms.PictureBox blissSymbol18;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.PictureBox blissSymbol19;
        private System.Windows.Forms.PictureBox blissSymbol21;
        private System.Windows.Forms.PictureBox blissSymbol22;
        private System.Windows.Forms.PictureBox blissSymbol23;
        private System.Windows.Forms.PictureBox blissSymbol24;
        private System.Windows.Forms.PictureBox playSymbol;
        private System.Windows.Forms.PictureBox volumeDecrease;
        private System.Windows.Forms.PictureBox volumeIncrease;
        private System.Windows.Forms.PictureBox mute;
        private System.Windows.Forms.PictureBox settings;
        private System.Windows.Forms.PictureBox assistance;
        private System.Windows.Forms.PictureBox privacey;
        private System.Windows.Forms.PictureBox clearMessage;
        private System.Windows.Forms.PictureBox displayBliss1;
        private System.Windows.Forms.PictureBox displayBliss2;
        private System.Windows.Forms.PictureBox displayBliss3;
        private System.Windows.Forms.PictureBox displayBliss4;
        private System.Windows.Forms.PictureBox displayBliss5;
        private System.Windows.Forms.PictureBox displayBliss6;
        private System.Windows.Forms.PictureBox displayBliss7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel symbol1;
        private System.Windows.Forms.Panel symbol2;
        private System.Windows.Forms.Panel symbol3;
        private System.Windows.Forms.Panel symbol4;
        private System.Windows.Forms.Panel symbol5;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel symbol19;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel symbol8;
        private System.Windows.Forms.Panel symbol11;
        private System.Windows.Forms.Panel symbol15;
        private System.Windows.Forms.Panel symbol16;
        private System.Windows.Forms.Panel symbol17;
        private System.Windows.Forms.Panel symbol6;
        private System.Windows.Forms.Panel symbol7;
        private System.Windows.Forms.Panel symbol9;
        private System.Windows.Forms.Panel symbol10;
        private System.Windows.Forms.Panel symbol12;
        private System.Windows.Forms.Panel symbol13;
        private System.Windows.Forms.Panel symbol14;
        private System.Windows.Forms.PictureBox blissSymbol10;
        private System.Windows.Forms.PictureBox blissSymbol20;
        private System.Windows.Forms.PictureBox symbol20;
        private System.Windows.Forms.Panel symbol18;
        private System.Windows.Forms.Panel panel20;
    }
}

