﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace group_poject_v2
{
    class BlissSymbols
    {
        public string  ID { get; set; } 
        public string  English { get; set; }
        public string  POS { get; set; }
        public string  Group { get; set; }
        public IList<Associated> Associations = new List<Associated>();
        public int SelectedPosition { get; set; }

        public string GetWord()
        {
            string sentence = this.English;
            sentence = sentence.Replace('_', ' ');
            sentence = sentence.Replace('-', ' ');

            char[] SplitChars = {',', '(' };

            string[] sentenceArray = sentence.Split(SplitChars);

            return sentenceArray[SelectedPosition];
        }
    }
}

/*
     @"{
          'BCI-AV': 14164,
          'English': 'feeling,emotion,sensation',
          'POS': 'YELLOW',
          'group': '0',
        }"
*/